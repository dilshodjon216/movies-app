import 'package:flutter/material.dart';
import 'package:movies_app/model/movie_response.dart';
import 'package:movies_app/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieSimilarBloc {
  final MovieRepository _response = MovieRepository();
  final BehaviorSubject<MovieResponse> _subject =
      BehaviorSubject<MovieResponse>();

  getSimilarMoves(int id) async {
   MovieResponse response = await _response.getSimilarMovies(id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<MovieResponse> get subject => _subject;
}

final movieSimilarBloc = MovieSimilarBloc();
